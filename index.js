const URL = "https://localhost:8030";
let valInterval = 42;

$(document).ready(function () {
    $('#submit').click(() => {

        clearInterval();
        $('#error').hide();
        $('#details').hide();
        $('#stop').hide();

        getCurrentPosition()
            .then((position) => {
                if (position.coords) {
                    $.ajax({
                        url: URL + "/letsGo",
                        type: "GET",
                        data: {
                            latOrg: position.coords.latitude,
                            longOrg: position.coords.longitude,
                            latDest: $('#latitude').val(),
                            longDest: $('#longitude').val()
                        },
                        success: (result) => {
                            $('#details').show();
                            $('#stop').show();
                            $('#error').hide();

                            $('#remainingMin').html(result.duration);
                            $('#remainingStopCount').html(result.stopCount);
                            $('#remainingDistance').html(result.lengthTrip);
                            $('#busNumber').html(result.busNumber);

                            poolingForChanges(result.lengthTrip);
                        },
                        error: (error) => {
                            //TODO: Better handling
                            $('#error').show();
                            $('#details').hide();
                            $('#stop').hide();
                            console.log("Error " + error);
                        }
                    });
                } else {
                    alert('Geolocation is not supported by this browser.');
                }
            })
            .catch((error) => {
                    var msg = null;
                    switch (error.code) {
                        case error.PERMISSION_DENIED:
                            msg = "User denied the request for Geolocation.";
                            break;
                        case error.POSITION_UNAVAILABLE:
                            msg = "Location information is unavailable.";
                            break;
                        case error.TIMEOUT:
                            msg = "The request to get user location timed out.";
                            break;
                        case error.UNKNOWN_ERROR:
                            msg = "An unknown error occurred.";
                            break;
                    }
                    alert(msg);
                }
            );
    });

    $('#stopTrip').click(() =>  {
        clearInterval(valInterval);
        $('#stop').hide();
        $('#details').hide();
        $('#error').hide();

    });

});

function getCurrentPosition() {
    if (navigator.geolocation) {
        return new Promise(
            (resolve, reject) => navigator.geolocation.getCurrentPosition(resolve, reject)
        )
    } else {
        return new Promise(
            resolve => resolve({})
        )
    }
}

function poolingForChanges(initialLength) {

    let halfWay = initialLength / 2;
    let twoThird = initialLength * 2 / 3;
    let reallyNear = 400;

    let patrn1Sent = false;
    let patrn2Sent = false;
    let patrn3Sent = false;

    valInterval = setInterval(() => {
        getCurrentPosition() //TODO: Code is sooo similar to above, should be in a single method
            .then((position) => {
                if (position.coords) {
                    $.ajax({
                        url: URL + "/letsGo",
                        type: "GET",
                        data: {
                            latOrg: position.coords.latitude,
                            longOrg: position.coords.longitude,
                            latDest: $('#latitude').val(),
                            longDest: $('#longitude').val()
                        },
                        success: (result) => {
                            $('#remainingMin').html(result.duration);
                            $('#remainingStopCount').html(result.stopCount);
                            $('#remainingDistance').html(result.lengthTrip);
                            $('#lastUpdate').html(Date().toString());
                            //TODO: Make this much smarter, using socket.io for instance
                            if (!!patrn1Sent && result.lengthTrip <= halfWay && result.lengthTrip > twoThird) {
                                vibratePhone("pat1");
                                patrn1Sent = true;
                            } else if (!!patrn2Sent && result.lengthTrip <= twoThird && result.lengthTrip > reallyNear) {
                                vibratePhone("pat2");
                                patrn2Sent = true;
                            } else if (!!patrn3Sent && result.lengthTrip <= reallyNear) {
                                vibratePhone("pat3");
                                patrn3Sent = true;
                            }
                        },
                        error: (error) => {
                            //TODO: Better handling
                            console.log("Error " + error);
                        }
                    });
                } else {
                    alert('Geolocation is not supported by this browser.');
                }
            })
            .catch((error) => {
                var msg = null;
                switch (error.code) {
                    case error.PERMISSION_DENIED:
                        msg = "User denied the request for Geolocation.";
                        break;
                    case error.POSITION_UNAVAILABLE:
                        msg = "Location information is unavailable.";
                        break;
                    case error.TIMEOUT:
                        msg = "The request to get user location timed out.";
                        break;
                    case error.UNKNOWN_ERROR:
                        msg = "An unknown error occurred.";
                        break;
                }
                alert(msg);
            }

    )}, 15000);

}

function vibratePhone(patternNumber) {
    var patterns = {
        "pat0": [],
        "pat1": [500, 2000, 500, 2000, 500],
        "pat2": [500, 1000, 500, 1000, 500],
        "pat3": [500, 250, 500, 250, 500]
    };

    if (patterns.hasOwnProperty(patternNumber)) {
        console.log(patterns[patternNumber]);
        navigator.vibrate(patterns[patternNumber]);
    }
}
