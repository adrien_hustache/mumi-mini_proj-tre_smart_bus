const express = require('express');
const app = express();
const request = require('request');
const https = require('https');
const fs = require('fs');
const moment = require('moment');

const port = 8030;


app.use(express.static(__dirname));


app.get('/letsGo', (req, res) => {
    let longOrg = req.query.longOrg;
    let latOrg = req.query.latOrg;
    let latDest = req.query.latDest;
    let longDest = req.query.longDest;

    request.get({
            url: 'http://api.publictransport.tampere.fi/prod/',
            qs: {
                user: '<YOUR USERNAME>',
                pass: '<YOUR PASSKEY>',
                request: 'route',
                epsg_in: 'wgs84',
                epsg_out: 'wgs84',
                from: longOrg + ',' + latOrg,
                to: longDest + ',' + latDest,
                date: moment().format('YYYYMMDD'),
                time: moment().format('hhmm'),
                optimize: 'fastest',
                change_margin: 8,
                show: 1,
                detail: 'full',
                format: 'json'
            }
        },
        function (error, response, body) {
            if (!error && response.statusCode === 200) {
                let result = JSON.parse(body);
                let stopCount = 0;
                let busNumber = "-1";
                result[0][0].legs.forEach(leg => {
                    if (leg.type !== "1") {
                        return;
                    }
                    stopCount = leg.locs.length;
                    busNumber = leg.code;
                });
                let theLength = result[0][0].length;
                let response = {
                    duration: moment.duration(result[0][0].duration, "seconds").asMinutes(),
                    stopCount,
                    lengthTrip: Math.round((theLength / 1000) * 100) / 100,
                    busNumber
                };
                res.json(response);
            } else {
                if (error) {
                    console.error(error.stack);
                }
                res.status(500).send('Something broke!');
            }
        }
    );


});

const httpsOptions = {
    key: fs.readFileSync('./security/key.pem'),
    cert: fs.readFileSync('./security/cert.pem')
};

const server = https.createServer(httpsOptions, app).listen(port, () => {
    console.log('Server running at ' + port)
});