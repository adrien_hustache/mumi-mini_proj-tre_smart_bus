# MUMI Mini projet - Tampere Smart Bus
Adrien Hustache, Genis Frigola, Ken JM Geelhoed

##How to start the application
Inside the repo `npm install` and then `npm start`

##API key
To access the public transport API, an account must be created [here](http://developer.publictransport.tampere.fi/pages/en/account-request.php) and the user token and passphrase generated must be filled on the server.js file (l.23-24)